﻿using System.Collections.Generic;
using System.Collections;
using System.Reflection;
using UnityEngine;
using System.Linq;
using UnityEditor;
using System;

namespace MassiveStudio.BetterInspector
{
    public class HideInBetterInspector : Attribute
    {
        public HideInBetterInspector() { }
    }

    public class BetterInspectorButton : Attribute
    {
        public MethodInfo method;
        public string titel;
        public BetterInspectorButton(Type t, string Method, string titel)
        {
            this.method = t.GetMethod(Method);
            this.titel = titel;
        }
    }

    public interface ISaveable
    {
        void SetObjectDirty();
    }

    public class PropertyDrawerUtility
    {
        public static T GetActualObjectForSerializedProperty<T>(FieldInfo fieldInfo, SerializedProperty property) where T : class
        {
            var obj = fieldInfo.GetValue(property.serializedObject.targetObject);
            if (obj == null)
            {
                Debug.Log("NULL " + fieldInfo.Name);
                return null;
            }
            else
            {
                // Debug.Log("NOTNULL " +obj.GetType());
            }

            T actualObject = null;
            if (obj.GetType().IsArray)
            {
                var index = Convert.ToInt32(new string(property.propertyPath.Where(c => char.IsDigit(c)).ToArray()));
                actualObject = ((T[])obj)[index];
            }
            else
            {
                actualObject = obj as T;
            }
            return actualObject;
        }
    }

    public class BetterInspector : EditorWindow
    {
        public static Dictionary<Type, Editor> cachedEditors = new Dictionary<Type, Editor>();

        public static BetterInspector _instance;
        public static BetterInspector instance
        {
            get
            {
                if (_instance == null)
                    _instance = CreateInstance<BetterInspector>();
                return _instance;
            }
            set
            {
                _instance = value;
            }
        }

        public object selectedObject = null;
        public Editor currentEditor = null;

        FieldInspector _betterInspector;
        FieldInspector betterInspector
        {
            get
            {
                if (_betterInspector == null)
                    _betterInspector = CreateInstance<FieldInspector>();
                return _betterInspector;
            }
            set
            {
                _betterInspector = value;
            }
        }

        [MenuItem("MassiveStudio/Inspector")]
        public static void Init()
        {
            BetterInspector window = GetWindow<BetterInspector>();
            window.betterInspector = CreateInstance<FieldInspector>();
            window.Show(true);
            window.titleContent = new GUIContent("BetterInspector");
            instance = window;
        }

        public void OnGUI()
        {
            if (selectedObject != null)
            {
                if (currentEditor != null)
                {
                    currentEditor.DrawHeader();
                    currentEditor.DrawDefaultInspector();
                }
                else
                {
                    if (betterInspector.DrawInspector(Screen.width, selectedObject))
                        Repaint();
                }
            }
        }

        public static void DrawObject(Rect rect, object obj, bool showAllFields, Action repaint)
        {
            if (instance.betterInspector.DrawInspector((int)rect.width, obj, true, showAllFields, false) && repaint != null)
                repaint();
        }

        public static void DrawObject(Rect rect, object obj)
        {
            instance.betterInspector.DrawInspector((int)rect.width, obj, false, false, false, rect.y);
        }

        public static void DrawField(ref Rect rect, Type t, IList field)
        {
            FieldInspector.ResetDeph();

            if (!instance.betterInspector.isinit)
                instance.betterInspector.InitInspector(false, false);

            rect.height = FieldInspector.height;

            FieldInspector.FieldDef fields = new FieldInspector.FieldDef();
            //if (instance.betterInspector.type_cache.TryGetValue(t, out fields))
            //{
            rect = instance.betterInspector.DrawList(t, field, rect, fields);
            //}
        }
        public static void DrawSingleField(ref Rect rect, object o, string fieldname)
        {
            if (o == null)
                return;

            FieldInspector.ResetDeph();
            if (!instance.betterInspector.isinit)
                instance.betterInspector.InitInspector(false, false);
            rect.height = FieldInspector.height;

            rect = instance.betterInspector.DrawSingleField(ref rect, o.GetType(), o, fieldname);
        }


        public static bool DrawObject(object obj)
        {
            instance.betterInspector.DrawInspector(Screen.width, obj, false, false);
            if (instance.betterInspector.changed)
            {
                if (obj is ISaveable)
                    (obj as ISaveable).SetObjectDirty();
                return true;
            }
            return false;
        }

        public void ReInitCache()
        {
            Editor editor;
            if (cachedEditors.TryGetValue(selectedObject.GetType(), out editor))//Draw CustomInspector
                currentEditor = editor;
            else
                currentEditor = null;
        }

        public void DrawHeader()
        {

        }

        public void OnSelectionChange()
        {
            Debug.Log("SELECTION CHANGED");
            selectedObject = Selection.activeObject;
            if (selectedObject != null)
                ReInitCache();
        }
    }

    public class FieldInspector : Editor
    {
        [SerializeField]
        private static Dictionary<Type, List<FieldDef>> stype_cache = new Dictionary<Type, List<FieldDef>>();

        [SerializeField]
        private static Dictionary<string, bool> typeHide_cache = new Dictionary<string, bool>();

        [SerializeField]
        private Dictionary<Type, List<FieldDef>> _type_cache;
        public Dictionary<Type, List<FieldDef>> type_cache
        {
            get
            {
                _type_cache = stype_cache;
                return stype_cache;
            }
            set
            {
                stype_cache = value;
                _type_cache = stype_cache;
            }
        }

        static int deph;

        public static void ResetDeph()
        {
            deph = 0;
        }

        public static bool GetState(Type t)
        {
            bool hide = false;
            if (!typeHide_cache.TryGetValue(deph + ":" + t.Name, out hide))
                typeHide_cache.Add(deph + ":" + t.Name, false);
            return hide;
        }

        public static void SetState(Type t, bool hide)
        {
            typeHide_cache[deph + ":" + t.Name] = hide;
        }

        public struct FieldDef
        {
            public FieldInfo field;
            public DrawType gtype;//Sample List<x>, Dictionary<x,y>
            public DrawType gtype2;//x
                                   //public Type arrayType;
            public FieldAction faction;//For DrawType.Button
            public bool isHidden;

            public FieldDef(FieldInfo info, DrawType type, DrawType type2 = DrawType.None, bool hide = false, FieldAction faction = null)
            {
                this.gtype = type;
                this.gtype2 = type2;
                this.field = info;
                //arrayType = null;
                this.isHidden = hide;
                this.faction = faction;
            }
        }

        public class FieldAction
        {
            public MethodInfo t;
            public string n;

            public FieldAction(MethodInfo action, string name)
            {
                this.t = action;
                this.n = name;
            }
        }

        public enum DrawType
        {
            None,
            ObjectField,
            CharField,
            IntField,
            ShortField,
            LongField,
            DoubleField,
            FloatField,
            StringField,
            BoolField,
            EnumField,
            ClassField,
            ClassArrayField,
            ClassListField,
            Dictionary,
            Vector3,
            Vector2,
            Color32,
            Color,
            Byte,
            AnimationCurve,
            Rect,

            Button,
            Vector4
        }

        public static float height = 50;//18

        static GUIStyle _style;
        static GUIStyle style
        {
            get
            {
                if (_style == null)
                {
                    _style = new GUIStyle();
                    _style.fontSize = 50;
                }
                return _style;
            }
        }

        static GUISkin _skin;
        static GUISkin sskin
        {
            get
            {
                if (_skin == null)
                {
                    _skin = CreateInstance<GUISkin>();
                    _skin.button = style;
                    _skin.label = style;
                    _skin.textField = style;
                    //_skin.textField.fontSize = 50;
                    _skin.textArea = style;
                    _skin.box = style;
                }
                return _skin;
            }
        }

        public Vector2 scroll;

        public static HashSet<Type> baseTypes = new HashSet<Type>()
    {
        typeof(string), typeof(int), typeof(float), typeof(char), typeof(byte), typeof(short), typeof(long), typeof(long), typeof(double), typeof(bool), typeof(uint), typeof(ulong), typeof(sbyte), typeof(ushort)
    };

        public GUIStyle addComponentButtonStyle;

        public static float zoom = 1f;

        #region LayoutSystem
        private static List<Rect> backgroundLayout = new List<Rect>();
        private static List<int> layouts = new List<int>();
        private static int reclayoutIndex = 0;
        private static int layoutIndex = 0;
        private bool useBackgroundLayout = true;
        private bool showHiddenElements = true;
        public Rect contentSize = new Rect();
        public bool changed = false;

        public bool isinit = false;

        public bool Zoom()
        {
            if (e.type == EventType.ScrollWheel && e.control)
            {
                //Debug.Log("ZOOM");
                if (e.delta.y != 0f)
                {
                    zoom += e.delta.y / 10f;
                    zoom = Mathf.Clamp(zoom, 0.1f, 10f);

                    int fontSize = Mathf.RoundToInt(18f * zoom);

                    sskin.scrollView.fontSize = fontSize;
                    sskin.box.fontSize = fontSize;
                    sskin.button.fontSize = fontSize;
                    sskin.label.fontSize = fontSize;
                    sskin.textArea.fontSize = fontSize;
                    sskin.textField.fontSize = fontSize;
                    sskin.toggle.fontSize = fontSize;
                    for (int i = 0; i < sskin.customStyles.Length; i++)
                        sskin.customStyles[i].fontSize = fontSize;

                    Repaint();

                    return true;
                }
            }
            return false;
        }

        public void Init()
        {
            reclayoutIndex = 0;
            layoutIndex = 0;
        }

        public void BeginLayoutGroup(Rect r)
        {
            if (e.type != EventType.Layout || !useBackgroundLayout)
                return;

            if (layouts.Count <= layoutIndex)
                layouts.Add(reclayoutIndex);
            else
                layouts[layoutIndex] = reclayoutIndex;
            layoutIndex++;


            if (backgroundLayout.Count <= reclayoutIndex)
                backgroundLayout.Add(r);
            else
                backgroundLayout[reclayoutIndex] = r;
            reclayoutIndex++;
            //backgroundLayout.Add(r);
        }

        public void EndLayoutGroup(Rect r)
        {
            if (e.type != EventType.Layout || !useBackgroundLayout)
                return;

            if (layoutIndex == 0)
                throw new Exception("You have to use BeginLayoutGroup before you can close a LayoutGroup");

            Rect or = backgroundLayout[layouts[layoutIndex - 1]];
            or.height = (r.y - or.y) + r.height;

            backgroundLayout[layouts[layoutIndex - 1]] = or;

            //layouts.Remove(layouts.Count-1);
            layoutIndex--;
        }

        public void DrawBackFields()
        {
            //GUI.Box(new Rect(50, 10, 200, 100), "LayoutRects: "+ backgroundLayout.Count+"\n" + reclayoutIndex+"/"+ layoutIndex+" : "+ useBackgroundLayout);
            for (int i = 0; i < reclayoutIndex; i++)
            {
                GUI.Box(backgroundLayout[i], "");
                //GUI.Box(new Rect(100,600+ i*50, 300,50), "Rect " + backgroundLayout[i].ToString());
            }
        }
        #endregion
        static Event e => Event.current;
        
        public float GetLayoutHeight()
        {
            if (backgroundLayout != null && backgroundLayout.Count > 0)
                return backgroundLayout[0].height;
            return height;
        }

        /// <summary>
        /// Draws the fields of @object
        /// </summary>
        /// <param name="width">width of the InspectorView</param>
        /// <param name="object">object to inspect</param>
        /// <param name="useBackgroundLayout">[optional] useBackgroundLayout in Box form?</param>
        /// <param name="showHiddenItems">[optional] show Hidden Fields?</param>
        /// <param name="drawFirstFoldout">[optional] Draw the First element as Foldout</param>
        /// <returns>if the window needs a Repaint</returns>
        public bool DrawInspector(int width, object @object, bool useBackgroundLayout = true, bool showHiddenItems = true, bool drawFirstFoldout = true, float starty = 0)
        {
            changed = false;

            if (@object == null)
                return false;

            if (addComponentButtonStyle == null)
                addComponentButtonStyle = "LargeButton";

            //e = Event.current;
            deph = 0;

            this.useBackgroundLayout = useBackgroundLayout;
            this.showHiddenElements = showHiddenItems;
            if (_skin == null)
            {
                _skin = new GUISkin();

                sskin.box = new GUIStyle(GUI.skin.box);
                sskin.button = new GUIStyle(GUI.skin.button);
                sskin.label = new GUIStyle(GUI.skin.label);
                sskin.textArea = new GUIStyle(GUI.skin.textArea);
                sskin.textField = new GUIStyle(GUI.skin.textField);
                sskin.toggle = new GUIStyle(GUI.skin.toggle);

                sskin.customStyles = new GUIStyle[GUI.skin.customStyles.Length];
                for (int i = 0; i < GUI.skin.customStyles.Length; i++)
                    sskin.customStyles[i] = new GUIStyle(GUI.skin.customStyles[i]);
            }


            bool repaint = false;
            repaint = Zoom();

            //Story story = (Story)target;
            //StoryElements element;
            //FieldDef[] fields;
            //FieldDef field;


            if (e.type == EventType.Layout)
                Init();

            if (e.type == EventType.Repaint && useBackgroundLayout)
            {
                //GUI.Box(new Rect(50, 500, 200, 100), "TEST");
                DrawBackFields();
                backgroundLayout.Clear();
            }

            int fontSize = Mathf.RoundToInt(18 * zoom);
            //GUIStyle style = sskin.label;
            //style.fontSize = fontSize;

            //Save Orginal
            GUISkin skin = GUI.skin;

            GUI.skin = sskin;

            height = sskin.label.CalcSize(new GUIContent("T")).y;

            Rect r = new Rect(0, starty, width - 18, height);
            //scroll = GUI.VerticalSlider(new Rect(width - 18, 0, 0, height), scroll, 0, GetLayoutHeight());
            //scroll = GUI.BeginScrollView(r, scroll, contentSize);
            EditorGUI.BeginChangeCheck();
            r = DrawObject(@object.GetType(), @object, r, drawFirstFoldout);
            if (EditorGUI.EndChangeCheck())
            {
                if (@object is ScriptableObject)
                    EditorUtility.SetDirty((@object as ScriptableObject));
            }
            //contentSize = new Rect(0, 0,r.width + r.x, r.y +r.height);
            //GUI.EndScrollView();
            //Revert back
            GUI.skin = skin;

            //r.y += 100;

            //EditorGUI.HelpBox(new Rect(0,r.y+100, Screen.width, Screen.height- r.y + 100))

            /*foreach (KeyValuePair<Type, List<FieldDef>> a in type_cache)
            {
                EditorGUI.LabelField(r, "FIELDType: " + a.Key.FullName);
                r.y += height;
                for (int c = 0; c < a.Value.Count; c++)
                {
                    if (a.Value[c].field != null)
                        EditorGUI.LabelField(r, "\t" + a.Value[c].field.Name + " : " + a.Value[c].field.FieldType + " : " + a.Value[c].gtype + " : " + a.Value[c].isHidden);
                    else
                        EditorGUI.LabelField(r, "\tNull");
                    r.y += height;
                }
                r.y += height;
            }*/

            return repaint;
        }

        public void InitInspector(bool useBackgroundLayout, bool showHiddenItems)
        {
            changed = false;

            if (addComponentButtonStyle == null)
                addComponentButtonStyle = "LargeButton";

            deph = 0;

            this.useBackgroundLayout = useBackgroundLayout;
            this.showHiddenElements = showHiddenItems;
            if (_skin == null)
            {
                _skin = new GUISkin();

                sskin.box = new GUIStyle(GUI.skin.box);
                sskin.button = new GUIStyle(GUI.skin.button);
                sskin.label = new GUIStyle(GUI.skin.label);
                sskin.textArea = new GUIStyle(GUI.skin.textArea);
                sskin.textField = new GUIStyle(GUI.skin.textField);
                sskin.toggle = new GUIStyle(GUI.skin.toggle);

                sskin.customStyles = new GUIStyle[GUI.skin.customStyles.Length];
                for (int i = 0; i < GUI.skin.customStyles.Length; i++)
                    sskin.customStyles[i] = new GUIStyle(GUI.skin.customStyles[i]);
            }


            bool repaint = false;
            repaint = Zoom();

            //Story story = (Story)target;
            //StoryElements element;
            //FieldDef[] fields;
            //FieldDef field;


            if (e.type == EventType.Layout)
                Init();

            if (e.type == EventType.Repaint && useBackgroundLayout)
            {
                //GUI.Box(new Rect(50, 500, 200, 100), "TEST");
                DrawBackFields();
                backgroundLayout.Clear();
            }

            int fontSize = Mathf.RoundToInt(18 * zoom);
            //GUIStyle style = sskin.label;
            //style.fontSize = fontSize;

            //Save Orginal
            GUISkin skin = GUI.skin;

            GUI.skin = sskin;

            height = sskin.label.CalcSize(new GUIContent("T")).y;

            isinit = true;
        }

        private Rect DrawObject(Type t, object o, Rect r, bool foldout = true)
        {
            BeginLayoutGroup(r);
            deph++;
            List<FieldDef> fields;
            FieldDef field;
            GUI.changed = false;

            object newValue = null;

            bool s = true;

            if (foldout)
            {
                s = EditorGUI.Foldout(new Rect(r.x + 12, r.y, r.width - 12, r.height), GetState(o.GetType()), o.GetType().FullName);// field.field.Name + "  (" + field.field.FieldType.Name + ")");
                r.y += height;
                if (GUI.changed)
                {
                    SetState(o.GetType(), s);
                    GUI.changed = false;
                }
            }

            if (s)
            {
                r.x += 12;
                r.width -= 12;
                if (type_cache.TryGetValue(t, out fields))
                {
                    for (int c = 0; c < fields.Count; c++)
                    {
                        field = fields[c];

                        if (field.isHidden && !showHiddenElements)
                            continue;

                        /* if (r.height > height)
                             throw new System.Exception("NEW LINE");*/

                        object val = field.field.GetValue(o);
                        newValue = DrawField(ref r, field, val, field.field.Name, o);

                        r.y += height;

                        if (GUI.changed)
                        {
                            field.field.SetValue(o, newValue);
                            changed = true;
                            GUI.changed = false;
                        }
                    }
                }
                else
                {
                    GenerateTypeInfo(o.GetType());
                }
                r.x -= 12;
                r.width += 12;
            }
            EndLayoutGroup(r);
            return r;
        }

        public Rect DrawSingleField(ref Rect r, Type t, object value, string fieldToDraw)
        {
            List<FieldDef> fields;
            FieldDef field;
            if (type_cache.TryGetValue(t, out fields))
            {
                for (int c = 0; c < fields.Count; c++)
                {
                    field = fields[c];

                    if (field.field.Name != fieldToDraw)
                        continue;

                    /* if (r.height > height)
                         throw new System.Exception("NEW LINE");*/

                    object val = field.field.GetValue(value);
                    object newValue = DrawField(ref r, field, val, field.field.Name, value);

                    r.y += height;

                    if (val != newValue)
                    {
                        //Debug.Log("SET VALUE");
                        field.field.SetValue(value, newValue);
                        changed = true;
                        //GUI.changed = false;
                    }
                }
            }
            else
            {
                GenerateTypeInfo(value.GetType());
            }
            return r;
        }

        public object DrawField(ref Rect r, Type t, object value, string fieldname, object referenzObj)
        {
            BeginLayoutGroup(r);
            object newValue = value;

            List<FieldDef> fields;
            if (stype_cache.TryGetValue(t, out fields))//Multifield Type
            {
                for (int i = 0; i < fields.Count; i++)
                {
                    newValue = DrawField(ref r, fields[i], value, fieldname, referenzObj);
                    if (value != newValue)
                    {
                        fields[i].field.SetValue(referenzObj, newValue);
                    }
                }
            }
            else if (baseTypes.Contains(t))//Base type (string, int, float)
            {
                newValue = DrawField(ref r, new FieldDef(null, GetFieldDrawType(t)), value, fieldname, referenzObj);
            }

            EndLayoutGroup(r);

            return newValue;
        }

        public object DrawField(ref Rect r, FieldDef field, object value, string fieldname, object referenzObj)
        {
            BeginLayoutGroup(r);

            object newValue = value;
            switch (field.gtype)
            {
                case DrawType.ObjectField:
                    if (field.field != null)
                    {
                        UnityEngine.Object fieldObject = (UnityEngine.Object)value;
                        newValue = EditorGUI.ObjectField(r, fieldname, fieldObject, field.field.FieldType, true);
                        r.y += height;
                        if (fieldObject != null && !field.field.FieldType.Name.StartsWith("Unity"))
                        {
                            r.x += 12;
                            r.width -= 12;
                            r = DrawObject(field.field.FieldType, fieldObject, r);
                            r.x -= 12;
                            r.width += 12;
                        }

                        /*if (CreateInstanceMenu.ButtonMouseDown(new Rect(r.x + r.width - 50, r.y, 50, r.height), new GUIContent("change"), FocusType.Passive, addComponentButtonStyle))
                            if (CreateInstanceMenu.Show(new Rect(r.x + r.width - 300, r.y, 300, r.height), this, field.field.FieldType, CreateInstanceMenu.InspectedFieldType.UnityObject, field.field, referenzObj))
                                GUIUtility.ExitGUI()*/
                    }
                    else
                        EditorGUI.LabelField(r, "NULL OBJECT FIELD");//Should never happen
                    break;
                case DrawType.IntField:
                    newValue = EditorGUI.IntField(r, fieldname, (int)value);
                    break;
                case DrawType.FloatField:
                    newValue = EditorGUI.FloatField(r, fieldname, (float)value);
                    break;
                case DrawType.StringField:
                    newValue = EditorGUI.TextField(r, fieldname, (string)value);
                    break;
                case DrawType.BoolField:
                    newValue = EditorGUI.Toggle(r, fieldname, (bool)value);
                    break;
                case DrawType.Byte:
                    newValue = EditorGUI.IntField(r, fieldname, (byte)value);
                    break;
                case DrawType.Vector4:
                    newValue = EditorGUI.Vector4Field(r, fieldname, (Vector4)value);
                    if (r.x > 500)
                        r.y += height;
                    break;
                case DrawType.Vector3:
                    newValue = EditorGUI.Vector3Field(r, fieldname, (Vector3)value);
                    if (r.x > 500)
                        r.y += height;
                    break;
                case DrawType.Vector2:
                    newValue = EditorGUI.Vector2Field(r, fieldname, (Vector2)value);
                    if (r.x > 500)
                        r.y += height;
                    break;
                case DrawType.Rect:
                    newValue = EditorGUI.RectField(r, fieldname, (Rect)value);
                    r.y += height;
                    break;
                case DrawType.Color32:
                case DrawType.Color:
                    newValue = EditorGUI.ColorField(r, fieldname, (Color)value);
                    break;
                case DrawType.EnumField:
                    newValue = EditorGUI.EnumPopup(r, fieldname, (Enum)value);
                    break;
                case DrawType.AnimationCurve:
                    newValue = EditorGUI.CurveField(r, fieldname, (AnimationCurve)value);
                    break;
                case DrawType.ClassField:
                    //EditorGUI.LabelField(r,fieldname);
                    //r.y += height;
                    object no = value;
                    EditorGUI.LabelField(r, fieldname + ":", no != null ? "NOTNULL" : "NULL (" + field.ToString() + ")");
                    r.y += height;
                    if (no != null)
                    {
                        EditorGUI.LabelField(r, field.field.FieldType.Name + ": " + "(" + no.GetType().Name + ")");
                        r.y += height;
                        r.x += 12;
                        r.width -= 12;
                        r = DrawObject(no.GetType(), no, r);
                        r.x -= 12;
                        r.width += 12;
                        if (CreateInstanceMenu.ButtonMouseDown(new Rect(r.x + r.width - 50, r.y, 50, r.height), new GUIContent("change"), FocusType.Passive, addComponentButtonStyle))
                            if (CreateInstanceMenu.Show(new Rect(r.x + r.width - 300, r.y, 300, r.height), this, field.field.FieldType, CreateInstanceMenu.InspectedFieldType.Class, field.field, referenzObj))
                                GUIUtility.ExitGUI();
                    }
                    else
                        if (CreateInstanceMenu.ButtonMouseDown(new Rect(r.x + r.width - 50, r.y, 50, r.height), new GUIContent("+"), FocusType.Passive, addComponentButtonStyle))
                        if (CreateInstanceMenu.Show(new Rect(r.x + r.width - 300, r.y, 300, r.height), this, field.field.FieldType, CreateInstanceMenu.InspectedFieldType.Class, field.field, referenzObj))
                            GUIUtility.ExitGUI();
                    break;
                case DrawType.Dictionary:
                    var n = value as IDictionary;
                    Type[] args = field.field.FieldType.GetGenericArguments();

                    EditorGUI.LabelField(r, fieldname, "(" + "Dictionary<" + args[0].Name + ", " + args[1].Name + ">" + "):" + (n != null ? "NOTNULL" : "NULL ("));
                    r.y += height;
                    if (n != null)
                    {
                        r = DrawDictionary(field.field.FieldType, n, r, field);
                        if (AddDictionaryKeyMenu.ButtonMouseDown(new Rect(r.x + r.width - 50, r.y, 50, r.height), new GUIContent("+"), FocusType.Passive, addComponentButtonStyle))
                            if (AddDictionaryKeyMenu.Show(new Rect(r.x + r.width - 300, r.y, 300, r.height), this, field.field.FieldType, field, referenzObj))
                                GUIUtility.ExitGUI();
                    }
                    else
                        if (CreateInstanceMenu.ButtonMouseDown(new Rect(r.x + r.width - 50, r.y, 50, r.height), new GUIContent("+"), FocusType.Passive, addComponentButtonStyle))
                        if (CreateInstanceMenu.Show(new Rect(r.x + r.width - 300, r.y, 300, r.height), this, field.field.FieldType, CreateInstanceMenu.InspectedFieldType.Class, field.field, referenzObj))
                            GUIUtility.ExitGUI();
                    break;
                case DrawType.ClassListField:
                    var nos = value as IList;
                    int oldC = nos == null ? 0 : nos.Count;
                    int Count = oldC;
                    Count = EditorGUI.IntField(r, fieldname + " Count: ", Count);
                    r.y += height;
                    if (Count != oldC)
                    {
                        changed = true;
                        //if (oldC != Count)
                        //{
                        if (nos == null)
                        {
                            value = CreateInstanceMenu.CreateTypeInstance(field.field.FieldType);
                            field.field.SetValue(referenzObj, value);
                            nos = value as IList;//Load Initialized value
                        }
                        if (oldC < Count)
                            for (int i = oldC; i < Count; i++)
                                nos.Add(null);
                        else
                            for (int i = oldC; i > Count; i--)
                                nos.RemoveAt(i - 1);
                        // }
                        //GUI.changed = false;
                    }
                    if (nos != null)//NULL LIST
                        r = DrawList(field.field.FieldType, nos, r, field);//, nos, field.field, r);
                    break;
                case DrawType.Button:
                    if (CreateInstanceMenu.ButtonMouseDown(new Rect(r.x + r.width - 100, r.y, 100, r.height), new GUIContent(field.faction.n), FocusType.Passive, addComponentButtonStyle))
                    {
                        Invoke(field.faction.t, referenzObj);
                    }
                    r.y += height;
                    break;
                default:
                    break;
            }
            EndLayoutGroup(r);
            return newValue;
        }

        public object DrawField(ref Rect r, DrawType drawType, object value, string fieldname, Type FieldType)
        {
            BeginLayoutGroup(r);

            object newValue = value;
            switch (drawType)
            {
                case DrawType.ObjectField:
                    UnityEngine.Object fieldObject = (UnityEngine.Object)value;
                    newValue = EditorGUI.ObjectField(r, fieldname, fieldObject, FieldType, true);
                    r.y += height;
                    if (fieldObject != null && FieldType.Namespace != "UnityEngine")
                    {
                        r.x += 12;
                        r.width -= 12;
                        r = DrawObject(FieldType, fieldObject, r);
                        r.x -= 12;
                        r.width += 12;
                    }
                    break;
                case DrawType.IntField:
                    newValue = EditorGUI.IntField(r, fieldname, (int)value);
                    break;
                case DrawType.FloatField:
                    newValue = EditorGUI.FloatField(r, fieldname, (float)value);
                    break;
                case DrawType.StringField:
                    newValue = EditorGUI.TextField(r, fieldname, (string)value);
                    break;
                case DrawType.BoolField:
                    newValue = EditorGUI.Toggle(r, fieldname, (bool)value);
                    break;
                case DrawType.Byte:
                    newValue = EditorGUI.IntField(r, fieldname, (byte)value);
                    break;
                case DrawType.Vector4:
                    newValue = EditorGUI.Vector4Field(r, fieldname, (Vector4)value);
                    if (r.x > 500)
                        r.y += height;
                    break;
                case DrawType.Vector3:
                    newValue = EditorGUI.Vector3Field(r, fieldname, (Vector3)value);
                    if (r.x > 500)
                        r.y += height;
                    break;
                case DrawType.Vector2:
                    newValue = EditorGUI.Vector2Field(r, fieldname, (Vector2)value);
                    if (r.x > 500)
                        r.y += height;
                    break;
                case DrawType.Rect:
                    newValue = EditorGUI.RectField(r, fieldname, (Rect)value);
                    r.y += height;
                    break;
                case DrawType.Color32:
                case DrawType.Color:
                    newValue = EditorGUI.ColorField(r, fieldname, (Color)value);
                    break;
                case DrawType.EnumField:
                    newValue = EditorGUI.EnumPopup(r, fieldname, (Enum)value);
                    break;
                case DrawType.AnimationCurve:
                    newValue = EditorGUI.CurveField(r, fieldname, (AnimationCurve)value);
                    break;
                default:
                    break;
            }
            EndLayoutGroup(r);
            return newValue;
        }

        public static void Invoke(MethodInfo method, params object[] par)
        {
            object[] parameters = new object[par.Length - 1];
            for (int i = 0; i < parameters.Length; i++)
                parameters[i] = par[i];

            if (method.IsStatic)//first par ist obj
            {
                parameters = new object[par.Length - 1];
                for (int i = 0; i < parameters.Length; i++)
                    parameters[i] = par[i];
                Debug.Log("PAR TYPE " + par[0].GetType());
                method.Invoke(null, par);
            }
            else
            {
                method.Invoke(par[0], parameters);
            }
        }

        public Rect DrawArray(Type t, Array nos, Rect r, FieldDef currentField)
        {
            BeginLayoutGroup(r);

            deph++;

            //EditorGUILayout.Foldout(true, field.field.Name + "  (" + field.field.FieldType.Name + ")");
            GUI.changed = false;
            bool h = EditorGUI.Foldout(new Rect(r.x + 12, r.y, r.width - 12, r.height), GetState(t), "(LISTYPE) " + ": " + "   " + t.Name + nos.GetType().FullName);// field.field.Name + "  (" + field.field.FieldType.Name + ")");
            r.y += height;
            if (GUI.changed)
            {
                SetState(t, h);
                GUI.changed = false;
            }
            if (h && nos != null)
            {
                int ranks = nos.Rank;
                EditorGUI.LabelField(r, "Ranks: " + ranks + " Length: " + nos.Length);
                r.y += height;

                #region Debug
                /*
                if (!nos.GetType().IsGenericType || nos.GetType().GetGenericTypeDefinition() != typeof(List<>))
                    EditorGUI.LabelField(r, "NO ARGS ");// + nos.GetType().GetGenericTypeDefinition().Name);
                else
                    EditorGUI.LabelField(r, nos.GetType().GetGenericArguments()[0].FullName);
                r.y += height;
                */
                #endregion

                for (int index = 0; index < nos.Length; index++)
                {
                    if (nos.GetValue(index) != null)
                    {
                        EditorGUI.LabelField(r, "(" + index + ") " + nos.GetValue(index).GetType().FullName + ":");
                        r.y += height;

                        #region Debug
                        /*
                        if (!nos.GetValue(index).GetType().IsGenericType || nos.GetValue(index).GetType().GetGenericTypeDefinition() != typeof(List<>))
                            EditorGUI.LabelField(r, "NO ARGS ");// + nos.GetType().GetGenericTypeDefinition().Name);
                        else
                            EditorGUI.LabelField(r, "PARAM: " + nos.GetValue(index).GetType().GetGenericArguments()[0].FullName);
                        r.y += height;
                        */
                        #endregion

                        r.x += 12;
                        r.width -= 12;

                        var obj = nos.GetValue(index);

                        //if (obj.GetType().IsArray ||(obj.GetType().IsGenericType && obj.GetType() == typeof(List<>)))
                        //    r = DrawList(obj.GetType(), obj, r);
                        //else
                        r = DrawObject(obj.GetType(), obj, r);

                        r.x -= 12;
                        r.width += 12;
                    }
                    else
                    {
                        EditorGUI.LabelField(r, "NULL");
                        r.y += height;
                    }
                    //EditorGUILayout.LabelField(field.field.Name, "NULL");
                    //r.y += height;
                }
                //r.y += height;
            }

            EndLayoutGroup(r);
            return r;
        }

        public Rect DrawDictionary(Type t, IDictionary nos, Rect r, FieldDef currentField)
        {
            BeginLayoutGroup(r);

            if (nos.Keys != null)
            {
                EditorGUI.LabelField(r, "KEYS " + nos.Keys.Count);
                r.y += height;
            }

            if (nos.Values != null)
            {
                EditorGUI.LabelField(r, "VALUES " + nos.Values.Count);
                r.y += height;
            }
            /*
            IList keys = nos.Keys as IList;
            IList values = nos.Values as IList;

            Rect KeyRect = r;
            KeyRect.width /= 2f;

            Rect ValueRect = KeyRect;
            ValueRect.x += ValueRect.width;

            if (keys != null)
                for (int index = 0; index < keys.Count; index++)
                {
                    EditorGUI.LabelField(r, "(" + index + ") " + nos[index].ToString() + ":");
                    r.y += height;
                }
            else
            {
                EditorGUI.LabelField(r, "Dictionary NULL");
                r.y += height;
            }*/
            EndLayoutGroup(r);
            return r;
        }

        public Rect DrawList(Type t, IList nos, Rect r, FieldDef currentField)
        {
            BeginLayoutGroup(r);
            deph++;

            Type[] types = t.GetGenericArguments();
            string s = "";
            if (types.Length > 1)
                for (int ti = 0; ti < types.Length; ti++)
                    s += (s.Length > 0 ? " " : "") + types[ti].Name;
            else if (types.Length > 0)
                s = types[0].Name;
            else
            {
                return DrawArray(t, nos as Array, r, currentField);
                //(Debug.Log(t.Name + " has no Generic Arguments");
                //return r;
            }

            bool oldState = GetState(t);
            bool h = EditorGUI.Foldout(new Rect(r.x + 12, r.y, r.width - 12, r.height), oldState, "List<" + s + "> " + "(" + nos.Count + ")");// field.field.Name + "  (" + field.field.FieldType.Name + ")");
            r.y += height;

            if (oldState != h)
            {
                SetState(t, h);
            }
            if (h && nos != null)
            {
                r.x += 12;
                r.width -= 12;
                //EditorGUI.LabelField(r, "Length: " + nos.Count);
                //r.y += height;

                Type fieldT = types[0];//List Element Type // != nos.GetType();

                #region Debug
                /*
                if (!nos.GetType().IsGenericType || nos.GetType().GetGenericTypeDefinition() != typeof(List<>))
                    EditorGUI.LabelField(r, "NO ARGS " + nos.GetType().FullName);
                else
                {
                    EditorGUI.LabelField(r, "GenericParams: " + nos.GetType().GetGenericArguments()[0].FullName);
                    fieldT = nos.GetType().GetGenericArguments()[0];
                }
                r.y += height;
                */
                #endregion
                if (baseTypes.Contains(fieldT))
                {
                    /*if (currentField.field != null)
                    {
                        EditorGUI.LabelField(r, currentField.field.Name, "(BASETYPE)");
                        r.y += height;
                    }*/

                    object element;
                    for (int index = 0; index < nos.Count; index++)
                    {
                        element = nos[index];
                        if (element != null)
                        {
                            object newval = DrawField(ref r, fieldT, element, "", element);
                            r.y += height;
                            if (element != newval)
                            {
                                changed = true;
                                nos[index] = newval;
                                //.changed = false;
                            }
                        }
                        else
                        {
                            nos[index] = CreateInstanceMenu.CreateTypeInstance(fieldT);
                            r.y += height;
                        }
                    }
                }
                else
                {
                    //Debug.Log("DRAW LIST ELEMENTS");
                    object element;
                    for (int index = 0; index < nos.Count; index++)
                    {
                        element = nos[index];
                        if (element != null)
                        {
                            EditorGUI.LabelField(r, "(" + index + ") ");// + element.GetType().FullName + ":");
                                                                        //r.y += height;

                            r.x += 24;
                            r.width -= 24;

                            if (GetFieldDrawType(element.GetType()) != DrawType.ObjectField)
                            {
                                List<FieldDef> fields;
                                if (type_cache.TryGetValue(t, out fields))
                                {
                                    for (int c = 0; c < fields.Count; c++)
                                    {
                                        object oldval = fields[c].field.GetValue(element);
                                        object newval = DrawField(ref r, fields[c], oldval, "", element);
                                        if (oldval != newval)
                                        {
                                            changed = true;
                                            fields[c].field.SetValue(element, newval);
                                        }
                                    }
                                }
                                else
                                    r = DrawObject(element.GetType(), element, r);
                            }
                            else
                            {
                                object newval = DrawField(ref r, DrawType.ObjectField, element, element.GetType().FullName, element.GetType());
                                if (element != newval)
                                {
                                    changed = true;
                                    nos[index] = newval;
                                }
                            }

                            r.x -= 24;
                            r.width += 24;

                            if (CreateInstanceMenu.ButtonMouseDown(new Rect(r.x + r.width - 50, r.y, 50, r.height), new GUIContent("change"), FocusType.Passive, addComponentButtonStyle))
                                if (CreateInstanceMenu.Show(new Rect(r.x + r.width - 300, r.y, 300, r.height), this, fieldT, CreateInstanceMenu.InspectedFieldType.FieldInList, nos, index))
                                    GUIUtility.ExitGUI();
                        }
                        else
                        {
                            EditorGUI.LabelField(r, "NULL");
                            r.y += height;
                            if (CreateInstanceMenu.ButtonMouseDown(new Rect(r.x + r.width - 50, r.y, 50, r.height), new GUIContent("+"), FocusType.Passive, addComponentButtonStyle))
                                if (CreateInstanceMenu.Show(new Rect(r.x + r.width - 300, r.y, 300, r.height), this, fieldT, CreateInstanceMenu.InspectedFieldType.FieldInList, nos, index))
                                    GUIUtility.ExitGUI();
                            r.y += height;
                        }
                    }
                }
                r.x -= 12;
                r.width += 12;

            }

            EndLayoutGroup(r);
            return r;
        }

        public class CreateInstanceMenu : EditorWindow
        {
            public static CreateInstanceMenu instance;
            public Type baseType;
            public List<Type> inherianceTypes;
            public InspectedFieldType inspectedFieldType;
            public ScriptableObject prevEditor;

            public IList list;
            public int obj;

            public FieldInfo field;
            public object reference;

            public enum InspectedFieldType
            {
                FieldInList,
                Class,
                UnityObject
            }

            public CreateInstanceMenu()
            {
                instance = this;
            }

            private void Init(Rect buttonRect, Type t)
            {
                this.baseType = t;
                buttonRect = GUIToScreenRect(buttonRect);

                LoadTypes();

                base.ShowAsDropDown(buttonRect, new Vector2(buttonRect.width, 320f));
                base.Focus();
                //            this.m_Parent.AddToAuxWindowList();
                base.wantsMouseMove = true;
            }

            public void LoadTypes()
            {
                inherianceTypes = new List<Type>();
                Assembly asm = baseType.Assembly;
                Type[] types = asm.GetTypes();
                inherianceTypes.Add(baseType);
                for (int c = 0; c < types.Length; c++)
                {
                    if (types[c].IsSubclassOf(baseType))
                        inherianceTypes.Add(types[c]);
                }
            }

            internal static Rect GUIToScreenRect(Rect guiRect)
            {
                Vector2 vector = GUIUtility.GUIToScreenPoint(new Vector2(guiRect.x, guiRect.y));
                guiRect.x = vector.x;
                guiRect.y = vector.y;
                return guiRect;
            }

            internal static bool Show(Rect rect, ScriptableObject editor, Type t, InspectedFieldType ft, params object[] values)
            {

                if (instance != null)
                    instance.Close();
                CreateInstanceMenu.instance = ScriptableObject.CreateInstance<CreateInstanceMenu>();

                CreateInstanceMenu.instance.prevEditor = editor;

                CreateInstanceMenu.instance.inspectedFieldType = ft;

                if (ft == InspectedFieldType.FieldInList)
                {
                    CreateInstanceMenu.instance.list = (IList)values[0];
                    CreateInstanceMenu.instance.obj = (int)values[1];
                }
                else if (ft == InspectedFieldType.Class)
                {
                    CreateInstanceMenu.instance.field = (FieldInfo)values[0];
                    CreateInstanceMenu.instance.reference = (object)values[1];
                }
                CreateInstanceMenu.instance.Init(rect, t);
                return true;
            }

            internal static bool ButtonMouseDown(Rect rec, GUIContent content, FocusType focusType, GUIStyle style)
            {
                //int controlID = GUIUtility.GetControlID(EditorGUI.s_ButtonMouseDownHash, focusType, rec);
                return ButtonMouseDown(0, rec, content, style);
            }

            internal static bool ButtonMouseDown(int id, Rect rec, GUIContent content, GUIStyle style)
            {
                Event current = Event.current;
                EventType type = current.type;
                switch (type)
                {
                    case EventType.KeyDown:
                        if (GUIUtility.keyboardControl == id && current.character == ' ')
                        {
                            Event.current.Use();
                            return true;
                        }
                        return false;
                    case EventType.KeyUp:
                    case EventType.ScrollWheel:
                        if (type != EventType.MouseDown)
                        {
                            return false;
                        }
                        if (rec.Contains(current.mousePosition) && current.button == 0)
                        {
                            Event.current.Use();
                            return true;
                        }
                        return false;
                    case EventType.Repaint:
                        style.Draw(rec, content, id, false);
                        return false;
                }

                if (type != EventType.MouseDown)
                {
                    return false;
                }
                if (rec.Contains(current.mousePosition) && current.button == 0)
                {
                    Event.current.Use();
                    return true;
                }
                return false;
            }

            public void OnGUI()
            {
                Rect r = new Rect(0, 0, 200, 25);

                GUI.Label(r, "Type: " + baseType.Name);
                r.y += 25f;

                for (int i = 0; i < inherianceTypes.Count; i++)
                {
                    if (GUI.Button(r, inherianceTypes[i].Name))
                    {
                        if (inspectedFieldType == InspectedFieldType.FieldInList)
                        {
                            //Debug.Log("CREATE INSTANCE FROM A FIELDINLIST OF TPYE " + inherianceTypes[i].FullName+" : " +inherianceTypes[i].GetGenericArguments()[0].FullName);
                            list[obj] = CreateTypeInstance(inherianceTypes[i]);

                            if (prevEditor is EditorWindow)
                            {
                                ((EditorWindow)prevEditor).Focus();
                                ((EditorWindow)prevEditor).Repaint();
                            }
                            else if (prevEditor is Editor)
                            {
                                ((Editor)prevEditor).Repaint();
                            }

                            this.Close();
                            return;

                            /*
                            if (inherianceTypes[i].IsValueType)
                                list[obj] = Activator.CreateInstance(inherianceTypes[i]);
                            else
                            {
                                ConstructorInfo[] infos = inherianceTypes[i].GetConstructors();
                                for (int c = 0; c < infos.Length; c++)
                                {
                                    if (infos[c].GetParameters().Length == 0)
                                    {
                                        list[obj] = infos[c].Invoke(null);


                                    }
                                }
                                Debug.LogError("Type " + inherianceTypes[i].Name + " can't be intialized");
                            }*/
                        }
                        else if (inspectedFieldType == InspectedFieldType.Class)
                        {
                            field.SetValue(reference, CreateTypeInstance(inherianceTypes[i]));
                            if (reference is ISaveable)
                                (reference as ISaveable).SetObjectDirty();
                            this.Close();
                            return;
                        }
                    }
                    r.y += 25f;
                }
            }

            public static object CreateTypeInstance(Type t)
            {
                //Debug.Log("CREATE INSTANCE " + t.FullName);

                if (Type.GetTypeCode(t) == TypeCode.String)
                    return string.Empty;

                if (t.IsValueType)
                    return Activator.CreateInstance(t);
                else
                {
                    ConstructorInfo[] infos = t.GetConstructors();
                    for (int c = 0; c < infos.Length; c++)
                        if (infos[c].GetParameters().Length == 0)
                            return infos[c].Invoke(null);

                    try
                    {
                        ParameterInfo[] param = infos[0].GetParameters();
                        object[] initData = new object[param.Length];

                        for (int i = 0; i < initData.Length; i++)
                        {
                            if (param[i].ParameterType.IsValueType)
                                initData[i] = Activator.CreateInstance(param[i].ParameterType);
                            else
                                initData[i] = null;
                        }
                        return infos[0].Invoke(initData);
                    }
                    catch (Exception e)
                    {
                        Debug.LogError("Type " + t.Name + " can't be intialized " + e.Message + "\n" + e.StackTrace);
                    }
                }
                return null;
            }
        }

        public class AddDictionaryKeyMenu : EditorWindow
        {
            public GUIStyle addComponentButtonStyle;

            public static AddDictionaryKeyMenu addinstance = null;
            public Type baseType;
            public List<Type> inherianceTypes;
            public FieldInspector prevEditor;

            public IList list;
            public int obj;

            public FieldInfo field;
            public object reference;

            public FieldDef fdef;
            public object f1;
            public object f2;

            public Type tf1;
            public Type tf2;

            public void OnEnable()
            {
                if (addComponentButtonStyle == null)
                    addComponentButtonStyle = "LargeButton";
            }

            public float width;
            public AddDictionaryKeyMenu()
            {
                addinstance = this;
            }

            private void Init(Rect buttonRect, Type t)
            {
                this.baseType = t;
                buttonRect = GUIToScreenRect(buttonRect);

                f1 = null;
                f2 = null;

                Type[] ts = fdef.field.FieldType.GetGenericArguments();
                Debug.Log("FIELD " + fdef.field.FieldType.Name);
                Debug.Log("Key " + ts[0].Name);
                Debug.Log("Value " + ts[1].Name);
                tf1 = ts[0];
                tf2 = ts[1];

                width = buttonRect.width;
                base.ShowAsDropDown(buttonRect, new Vector2(buttonRect.width, 320f));
                base.Focus();
                //            this.m_Parent.AddToAuxWindowList();
                base.wantsMouseMove = true;
            }

            internal static Rect GUIToScreenRect(Rect guiRect)
            {
                Vector2 vector = GUIUtility.GUIToScreenPoint(new Vector2(guiRect.x, guiRect.y));
                guiRect.x = vector.x;
                guiRect.y = vector.y;
                return guiRect;
            }

            internal static bool Show(Rect rect, FieldInspector editor, Type t, params object[] values)
            {
                try
                {
                    addinstance?.Close();
                }
                catch (Exception)
                {

                }
                finally
                {
                    AddDictionaryKeyMenu.addinstance = ScriptableObject.CreateInstance<AddDictionaryKeyMenu>();

                    AddDictionaryKeyMenu.addinstance.prevEditor = editor;

                    //AddDictionaryKeyMenu.addinstance.field = (FieldInfo)values[0];
                    AddDictionaryKeyMenu.addinstance.fdef = (FieldDef)values[0];
                    AddDictionaryKeyMenu.addinstance.reference = (object)values[1];

                    AddDictionaryKeyMenu.addinstance.Init(rect, t);
                }
                return true;
            }

            internal static bool ButtonMouseDown(Rect rec, GUIContent content, FocusType focusType, GUIStyle style)
            {
                //int controlID = GUIUtility.GetControlID(EditorGUI.s_ButtonMouseDownHash, focusType, rec);
                return ButtonMouseDown(s_DropdownButtonHash, rec, content, style);
            }
            private static int s_DropdownButtonHash = "DropdownButton".GetHashCode();
            internal static bool ButtonMouseDown(int id, Rect rec, GUIContent content, GUIStyle style)
            {
                Event current = Event.current;
                EventType type = current.type;
                switch (type)
                {
                    case EventType.KeyDown:
                        if (GUIUtility.keyboardControl == id && current.character == ' ')
                        {
                            Event.current.Use();
                            return true;
                        }
                        return false;
                    case EventType.KeyUp:
                    case EventType.ScrollWheel:
                        if (type != EventType.MouseDown)
                        {
                            return false;
                        }
                        if (rec.Contains(current.mousePosition) && current.button == 0)
                        {
                            Event.current.Use();
                            return true;
                        }
                        return false;
                    case EventType.Repaint:
                        style.Draw(rec, content, id, false);
                        return false;
                }

                if (type != EventType.MouseDown)
                {
                    return false;
                }
                if (rec.Contains(current.mousePosition) && current.button == 0)
                {
                    Event.current.Use();
                    return true;
                }
                return false;
            }

            public void OnGUI()
            {
                Rect r = new Rect(0, 0, width, 25);

                GUI.Label(r, "Add Key: " + tf1.Name);
                r.y += 25f;
                f1 = prevEditor.DrawField(ref r, fdef, f1, "Key:", reference);
                /*if (CreateInstanceMenu.ButtonMouseDown(new Rect(r.x + r.width - 50, r.y, 50, r.height), new GUIContent("change"), FocusType.Passive, addComponentButtonStyle))
                    if (CreateInstanceMenu.Show(new Rect(r.x + r.width - 300, r.y, 300, r.height), this, tf1, CreateInstanceMenu.InspectedFieldType.FieldInList, nos, index))
                        GUIUtility.ExitGUI();*/

                GUI.Label(r, "Add Value: " + tf2.Name);
                r.y += 25f;
                f2 = prevEditor.DrawField(ref r, fdef, f2, "Value:", reference);


                GUI.Label(r, "Ty: " + tf1.Name + " / " + tf2.Name);
                //f1 = prevEditor.DrawField(ref r, null, fdef.gtype, f1, "Key:", reference);
                r.y += 25f;
                //f2 = prevEditor.DrawField(ref r, null, fdef.gtype, f2, "Value:", reference);
            }
        }

        public static IEnumerable<FieldInfo> GetAllFields(Type t)
        {
            if (t == null || t == typeof(UnityEngine.Object))
                return Enumerable.Empty<FieldInfo>();

            BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic |
                                 BindingFlags.Static | BindingFlags.Instance |
                                 BindingFlags.DeclaredOnly;
            return (GetAllFields(t.BaseType).Concat(t.GetFields(flags)));
        }//Reads all Fields from current Type and child Types

        public void GenerateTypeInfo(Type t, FieldInfo field = null)
        {

            TypeCode tc = Type.GetTypeCode(t);
            if (tc != TypeCode.Object && tc != TypeCode.Empty)//is basic value
            {
                DrawType dt = GetFieldDrawType(t);
                type_cache.Add(t, new List<FieldDef> { new FieldDef(null, dt, 0, false) });
                return;
            }

            if (t.IsArray || t is IList)
            {
                Debug.Log("IS ARRAY " + t.Name);
                bool isHidden = false;// t.GetCustomAttribute(typeof(HideInBetterInspector)) != null;
                type_cache.Add(t, new List<FieldDef> { new FieldDef(null, DrawType.ClassListField, 0, isHidden) });
                return;
            }
            else if (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Dictionary<,>))
            {
                //type_cache.Add(t, new[] { new FieldDef(null, GUIType.Dictionary, false) });
                Debug.LogError("ERROR DICTIONARY");
                return;
            }

            FieldInfo[] fields = GetAllFields(t).ToArray();// t.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance | BindingFlags.SetField | BindingFlags.GetField | BindingFlags.GetProperty | BindingFlags.SetProperty);
            FieldDef fd;


            List<FieldDef> fds = new List<FieldDef>();//[fields.Length];
            for (int i = 0; i < fields.Length; i++)
            {
                //Debug.Log("FIELD " + fields[i].Name + "   " + fields[i].FieldType.FullName);
                bool isHidden = fields[i].GetCustomAttribute(typeof(HideInBetterInspector)) != null;

                fd = new FieldDef(fields[i], GetFieldDrawType(fields[i].FieldType), 0, isHidden);

                //if (fd.gtype == DrawType.ClassListField)
                //{
                //    Debug.Log("ArrTy: " + fields[i].FieldType.Name);
                //fd.arrayType = fields[i].FieldType;//NEED THIS???
                //}

                #region outsourced -> GetFieldDrawType
                /*TypeCode code = Type.GetTypeCode(fields[i].FieldType);
                switch (code)
                {
                    case TypeCode.Empty:
                        continue;
                    case TypeCode.Object:
                        {
                            if (fields[i].FieldType == typeof(Vector3))
                                fd = new FieldDef(fields[i], DrawType.Vector3,0, isHidden);
                            else if (fields[i].FieldType == typeof(Vector2))
                                fd = new FieldDef(fields[i], DrawType.Vector2, 0, isHidden);
                            else if (fields[i].FieldType == typeof(Color32))
                                fd = new FieldDef(fields[i], DrawType.Color32, 0, isHidden);
                            else if (fields[i].FieldType == typeof(Color))
                                fd = new FieldDef(fields[i], DrawType.Color, 0, isHidden);
                            else if(fields[i].FieldType == typeof(AnimationCurve))
                                fd = new FieldDef(fields[i], DrawType.AnimationCurve, 0, isHidden);
                            else if (fields[i].FieldType.IsSubclassOf(typeof(UnityEngine.Object)))
                                fd = new FieldDef(fields[i], DrawType.ObjectField, 0, isHidden);
                            else if (fields[i].FieldType == typeof(Dictionary<,>))
                            {

                                fd = new FieldDef(fields[i], DrawType.Dictionary, 0, isHidden);
                            }
                            else if ((fields[i].FieldType == typeof(object) || (fields[i].FieldType.IsClass)) && (!fields[i].FieldType.IsGenericType && !fields[i].FieldType.IsArray && !(fields[i].FieldType is IList)))
                            {
                                // Debug.Log("FIELD " + fields[i].FieldType.FullName + " ISN'T a LIST/ARRAY     L: " + (fields[i].FieldType is IList) + "  A: " + fields[i].FieldType.IsArray);
                                fd = new FieldDef(fields[i], DrawType.ClassField, 0, isHidden);
                            }
                            else if (fields[i].FieldType.IsArray || fields[i].FieldType.IsGenericType || (fields[i].FieldType is IList))
                            {// && fields[i].FieldType.ty == typeof(object[]))
                             // Debug.Log("FIELDNAME " + fields[i].Name + "  ARRAY TYPE " + fields[i].FieldType.FullName);//MakeArrayType().
                             // Debug.Log("\t" + "  ARRAY TYPE " + fields[i].FieldType.FullName);
                             // Debug.Log("IS GENERIC " + t.Name + " IsDICTIONARY " + (fields[i].FieldType is IDictionary));

                                if (fields[i].FieldType.IsGenericType && fields[i].FieldType.GetGenericTypeDefinition() == typeof(Dictionary<,>))
                                {
                                    fd = new FieldDef(fields[i], DrawType.Dictionary, 0, isHidden);
                                }
                                else
                                {
                                    fd = new FieldDef(fields[i], DrawType.ClassListField, 0, isHidden);
                                    fd.arrayType = fields[i].FieldType;//.MakeArrayType(fields[i].FieldType.GetArrayRank());
                                }
                            }
                            else { Debug.LogError("FIELDTYPE: " + fields[i].FieldType.Name + " missing, please contract the administrator for fixing/adding it"); continue; }
                            /*else
                            {
                               //A Debug.Log("object FIELD " + fields[i].Name + "   " + fields[i].FieldType.FullName);
                                continue;
                            }*//*
                        }
                        break;
                    case TypeCode.DBNull:
                        continue;
                    case TypeCode.Boolean:
                        fd = new FieldDef(fields[i], DrawType.BoolField, 0, isHidden); break;
                    case TypeCode.Char:
                        fd = new FieldDef(fields[i], DrawType.CharField, 0, isHidden); break;
                    case TypeCode.SByte:
                    case TypeCode.Byte:
                        fd = new FieldDef(fields[i], DrawType.Byte, 0, isHidden); break;
                    case TypeCode.Int16:
                    case TypeCode.UInt16:
                        fd = new FieldDef(fields[i], DrawType.ShortField, 0, isHidden); break;
                    case TypeCode.Int32:
                    case TypeCode.UInt32:
                        fd = new FieldDef(fields[i], DrawType.IntField, 0, isHidden); break;
                    case TypeCode.Int64:
                    case TypeCode.UInt64:
                        fd = new FieldDef(fields[i], DrawType.LongField, 0, isHidden); break;
                    case TypeCode.Single:
                        fd = new FieldDef(fields[i], DrawType.FloatField, 0, isHidden); break;
                    case TypeCode.Double:
                        fd = new FieldDef(fields[i], DrawType.DoubleField, 0, isHidden); break;
                    case TypeCode.Decimal:
                    case TypeCode.DateTime:
                    case TypeCode.String:
                        fd = new FieldDef(fields[i], DrawType.StringField, 0, isHidden); break;
                    default:
                        Debug.LogError("FIELDTYPE Code: " + fields[i].FieldType.Name + " missing, please contract the administrator for fixing/adding it");
                        continue;
                }*/
                #endregion
                if (fd.field != null)
                    fds.Add(fd);
            }

            //fds = fds.Where(x => x.field != null).ToList();

            BetterInspectorButton customActionButton = (BetterInspectorButton)t.GetCustomAttribute(typeof(BetterInspectorButton));
            if (customActionButton != null)
            {
                fd = new FieldDef(fields[0], DrawType.Button, 0, false, new FieldAction(customActionButton.method, customActionButton.titel));
                Debug.Log("Type " + t.Name + " hat Button");
                fds.Add(fd);
            }

            type_cache.Add(t, fds);
        }

        public DrawType GetFieldDrawType(Type field)
        {
            if (field.IsEnum)
                return DrawType.EnumField;

            TypeCode code = Type.GetTypeCode(field);
            switch (code)
            {
                case TypeCode.Empty:
                    return DrawType.None;
                case TypeCode.Object:
                    {
                        if (field.IsInterface)
                        {
                            return DrawType.None;
                        }
                        else if (field.GetType().IsClass)
                        {
                            if (field == typeof(AnimationCurve))
                                return DrawType.AnimationCurve;
                            else if (field.IsSubclassOf(typeof(UnityEngine.Object)))
                                return DrawType.ObjectField;
                            else if (field == typeof(Dictionary<,>))
                                return DrawType.Dictionary;
                        }
                        else
                        {
                            if (field == typeof(Vector3))
                                return DrawType.Vector3;
                            else if (field == typeof(Vector2))
                                return DrawType.Vector2;
                            else if (field == typeof(Vector4))
                                return DrawType.Vector4;
                            else if (field == typeof(Color32))
                                return DrawType.Color32;
                            else if (field == typeof(Color))
                                return DrawType.Color;
                            else if (field == typeof(Rect))
                                return DrawType.Rect;
                        }

                        if ((field == typeof(object) || (field.IsClass)) && (!field.IsGenericType && !field.IsArray && !(field is IList)))
                            return DrawType.ClassField;
                        else if (field.IsArray || field.IsGenericType || (field is IList))
                        {
                            if (field.IsGenericType && field.GetGenericTypeDefinition() == typeof(Dictionary<,>))
                                return DrawType.Dictionary;
                            else
                                return DrawType.ClassListField;
                        }
                        else { Debug.LogError("FIELDTYPE: " + field.Name + " missing, please contract the administrator for fixing/adding it"); return DrawType.None; }
                    }
                case TypeCode.DBNull:
                    return DrawType.None;
                case TypeCode.Boolean:
                    return DrawType.BoolField;
                case TypeCode.Char:
                    return DrawType.CharField;
                case TypeCode.SByte:
                case TypeCode.Byte:
                    return DrawType.Byte;
                case TypeCode.Int16:
                case TypeCode.UInt16:
                    return DrawType.ShortField;
                case TypeCode.Int32:
                case TypeCode.UInt32:
                    return DrawType.IntField;
                case TypeCode.Int64:
                case TypeCode.UInt64:
                    return DrawType.LongField;
                case TypeCode.Single:
                    return DrawType.FloatField;
                case TypeCode.Double:
                    return DrawType.DoubleField;
                case TypeCode.Decimal:
                case TypeCode.DateTime:
                case TypeCode.String:
                    return DrawType.StringField;
                default:
                    Debug.LogError("FIELDTYPE Code: " + field.Name + " missing, please contract the administrator for fixing/adding it");
                    return DrawType.None;
            }
        }
    }

    [Serializable]
    public class SerialList<T, F> : IList<F>
    {
        public List<T> jsonData;
        [NonSerialized]
        public List<F> objectData;

        F IList<F>.this[int index]
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        F this[int index]
        {
            get
            {
                return objectData[index];
            }
            set
            {
                //jsonData[index] = jsonData...
                objectData[index] = value;
            }
        }

        public int Count
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public bool IsReadOnly
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public void Add(F item)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(F item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(F[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<F> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public int IndexOf(F item)
        {
            throw new NotImplementedException();
        }

        public void Insert(int index, F item)
        {
            throw new NotImplementedException();
        }

        public bool Remove(F item)
        {
            throw new NotImplementedException();
        }

        public void RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}